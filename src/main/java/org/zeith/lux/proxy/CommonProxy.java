package org.zeith.lux.proxy;

import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class CommonProxy
{
	public void preInit(FMLPreInitializationEvent e)
	{
	}

	public void postInit()
	{
	}

	public void reloadLuxManager()
	{
	}

	public float getFogIntensity()
	{
		return 1F;
	}
}